<?php

/**
 * Created by PhpStorm.
 * User: strrife
 * Date: 2/24/16
 * Time: 2:50 PM
 */
class Calculator
{
    const DEFAULT_DELIMITER = ",|\n";
    const MAX_NUMBER = 1000;
    const DELIMITER_DETECTION_REGEX = "/^\\/\\/(.+)\n/";

    public static function add($string){
        list($string, $delimiter) = self::processInput($string);
        if($string === '' || preg_match('/^(?:-?\\d+(?:' . $delimiter . '))*-?\\d+$/', $string)) {
            $numbers = array_map('intval', preg_split('/' . $delimiter . '/', $string));
            self::checkNegatives($numbers);
            return array_sum(
                array_filter($numbers, function($x) {  return $x <= self::MAX_NUMBER; })
            );
        } else {
            return null;
        }
    }

    protected static function getDelimiterFromCandidate($delimiterCandidate){
        if(preg_match('/^\[.+]$/', $delimiterCandidate)) {
            $delimiterCandidate = explode('][', substr($delimiterCandidate, 1, -1));
        }
        return self::DEFAULT_DELIMITER . "|" .
                implode('|',
                    array_map('preg_quote',
                        is_array($delimiterCandidate) ? $delimiterCandidate : [$delimiterCandidate]
                    )
                );
    }

    protected static function processInput($string){
        if(preg_match('/^\/\/(.+)\n/i', $string, $delimiterCandidate)) {
            $string = substr($string, strlen($delimiterCandidate[0]));
            return [ $string, self::getDelimiterFromCandidate($delimiterCandidate[1]) ];
        } else {
            return [$string, self::DEFAULT_DELIMITER];
        }

    }


    protected static function checkNegatives($numbers){
        $negatives = array_filter($numbers, function($e) { return $e < 0; });
        if($negatives && count($negatives)){
            throw new CalculatorException("Negatives not allowed", $negatives);
        }
    }

}