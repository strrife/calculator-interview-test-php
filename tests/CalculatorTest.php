<?php
/**
 * Created by PhpStorm.
 * User: strrife
 * Date: 2/24/16
 * Time: 2:52 PM
 */

require_once __DIR__ . '/../vendor/autoload.php';

class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    public function testTrue()
    {
        $this->assertEquals(true, true);
    }

    public function testAddZeroNumbers()
    {
        $this->assertEquals(Calculator::add(''), 0);
    }

    public function testAddOneNumber()
    {
        $this->assertEquals(Calculator::add('5'), 5);
    }

    public function testAddTwoNumber()
    {
        $this->assertEquals(Calculator::add('5,8'), 13);
    }

    public function testAddUnknownAmountOfNumbers()
    {
        $this->assertEquals(Calculator::add('1,2,3,4'), 10);
    }

    public function testMarkInputAsInvalid()
    {
        $this->assertEquals(Calculator::add('1,'), null);
    }

    public function testSupportNewlinesAsDelimiters()
    {
        $this->assertEquals(Calculator::add("1,2\n3"), 6);
    }

    public function testMarkInputAsInvalidWithTwoDelimiters()
    {
        $this->assertEquals(Calculator::add("1,\n"), null);
    }

    public function testNamdleCustomDelimiters()
    {
        $this->assertEquals(Calculator::add("//;\n1;2"), 3);
    }

    public function testBeOkWithRegexDelims()
    {
        $this->assertEquals(Calculator::add("//?\n1?2"), 3);
    }

    public function testSupportOldDelimitersWhenAddingNewOne()
    {
        $this->assertEquals(Calculator::add("//?\n1,2"), 3);
    }

    /**
     * @expectedException CalculatorException
     */
    public function testThrowAnExceptionOnNegatives()
    {
        Calculator::add('1,-2');
    }

    public function testSkipMoreThan1000()
    {
        $this->assertEquals(Calculator::add('1,1001'), 1);
    }

    public function testBeOkWithLongDelims()
    {
        $this->assertEquals(Calculator::add("//[***]\n1***2"), 3);
    }

    public function testBeOkWithMultipleLongDelims()
    {
        $this->assertEquals(Calculator::add("//[*][%%]\n1*2%%3"), 6);
    }
}
