<?php
/**
 * Created by PhpStorm.
 * User: strrife
 * Date: 2/24/16
 * Time: 3:14 PM
 */

class CalculatorException extends \Exception
{
    public $data;

    /**
     * CalculatorException constructor.
     * @param string $message
     * @param array $data
     */
    function __construct($message, $data)
    {
        parent::__construct($message);
        $this->data = $data;
    }
}